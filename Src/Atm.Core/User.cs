﻿using System.Collections.Generic;

namespace Atm.Core
{
    public class User
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public int LoginAttemptCount { get; set; }

        public bool IsLocked { get; set; }

        public AtmCard AtmCard { get; set; }

        public ICollection<Account> Accounts { get; set; }

        public User()
        {
            Accounts = new List<Account>();
        }
    }
}