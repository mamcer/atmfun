﻿namespace Atm.Core
{
    public class Account
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public User User { get; set; }
    }
}