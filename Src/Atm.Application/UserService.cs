﻿using Atm.Core;

namespace Atm.Application
{
    public class UserService : IUserService
    {
        public User GetById(int id)
        {
            var user = new User
            {
                UserName = "mario",
                IsLocked = false,
                AtmCard = new AtmCard { Pin = "1234", Number = "qwe" }
            };
            user.Accounts.Add(new Account { Amount = 1000 });

            return user;
        }
    }
}