﻿using Atm.Core;

namespace Atm.Application
{
    public class AtmCardService : IAtmCardService
    {
        public AtmCard GetCardByNumber(string cardNumber)
        {
            return new AtmCard { Pin = "1234", Number = "qwe", User = new User { IsLocked = false } };
        }

        public User ValidateAtmCardPin(string cardNumber, string pin)
        {
            var user = new User
            {
                UserName = "mario",
                IsLocked = false,
                AtmCard = new AtmCard { Pin = "1234", Number = "qwe" }
            };
            user.Accounts.Add(new Account { Amount = 1000 });

            return user;
        }
    }
}