using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using CrossCutting.MainModule.IOC;

namespace Atm.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            var unityContainer = new IocUnityContainer(container);


            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}